#include "RGBA.h"
	
uint8_t RGBAs::extractRed(RGBA color) {
	return (color >> 24) & 0xFF;
}

uint8_t RGBAs::extractGreen(RGBA color) {
	return (color >> 16) & 0xFF;
}

uint8_t RGBAs::extractBlue(RGBA color) {
	return (color >> 8) & 0xFF;
}

uint8_t RGBAs::extractAlpha(RGBA color) {
	return (color >> 0) & 0xFF;
}

RGBA RGBAs::create(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha) {
	return (((red >> 8) + green >> 8) + blue >> 8) + alpha;
}

