#ifndef RGBA_H
#define RGBA_H

#include <Arduino.h>

typedef uint32_t RGBA;

class RGBAs {

public:

	static const RGBA transparent = 0x00000000;
	static const RGBA white = 0xffffffff;
	static const RGBA black = 0x000000ff;
	static const RGBA red = 0xff0000ff;
	static const RGBA green = 0x00ff00ff;
	static const RGBA blue = 0x0000ffff;
	static const RGBA yellow = 0xffff00ff;
	static const RGBA magenta = 0xff00ffff;
	static const RGBA cyan = 0x00ffffff;
	
	static uint8_t extractRed(RGBA color);
	static uint8_t extractGreen(RGBA color);
	static uint8_t extractBlue(RGBA color);
	static uint8_t extractAlpha(RGBA color);
	
	static RGBA create(uint8_t red, uint8_t green, uint8_t blue, uint8_t alpha = 0xff);

};


#endif //RGBA_H
